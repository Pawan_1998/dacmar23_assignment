import java.util.Scanner;

/**
 * Percentage
 */
public class Percentage {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("   *** \"Percentage Calculator\" *** ");
        System.out.println("=====================================");
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter the TOTAL MARKS per Subject : ");
        float t = sc.nextFloat();

        System.out.print("Enter the Marks Obtend in Subject 1 :");
        float a = sc.nextFloat();

        System.out.print("Enter the Marks Obtend in Subject 2 : ");
        float b = sc.nextFloat();

        System.out.print("Enter the Marks Obtend in Subject 3 : ");
        float c = sc.nextFloat();

        System.out.print("Enter the Marks Obtend in Subject 4 : ");
        float d = sc.nextFloat();

        System.out.print("Enter the Marks Obtend in Subject 5 : ");
        float e = sc.nextFloat();

        float Per = ((a + b + c + d + e)*100)/(5*t);

        System.out.printf("Total Percentage obtend : %.2f ", Per);
        System.out.println(" % ");
      sc.close();
    }

}
